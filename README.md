# Nanodroid - microprocessor development board #
* https://embeto.com/nanodroid/
* Small and complete development board based on the ATmega328P.
* Factory loaded Arduino Nano bootloader and therefore can be used with the Arduino IDE or as a standalone ATmega328P board. 
* ISP connector and a programmer used to flash the program memory (bear in mind the risk of altering the boot flash section!). 
* On-board 16 Mhz crystal and voltage regulator for external power supplies with voltages up to 12V. 

## Repository Structure ##

* 3Ddesign
	*  computer rendered figures of the Nanodroid board

* documents 
	* datasheets - misc files used in the development process
	* leaflet - product leaflet given with the dev board and containing board pinout, functions and detailed description 
	* schematics - each tapeout version is listed here in a directory containing the corresponding BOM and PDF schematic
	* drawings - blueprints of board exported from AUTODESK Fusion 360

* pcbDesign
	* mainModule
		* schematics - Working directory for mainModule circuit board
		* tapeouts (see *circuit Board design* section for more)
	
## Circuit Board design ##

* mainModule
	* v1.1 - (factory produced) first version of nanodroid microprocessor development board

## 3D design ##

* view/download the 3D enclosure design in Web Browser [here](https://a360.co/2IYAgEc)